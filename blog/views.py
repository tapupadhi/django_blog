from datetime import datetime
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseNotFound
from django.utils import timezone
from django.views import View
from blog.models import ArticleModel
from blog.forms import ArticleForm


# Create your views here.
class Home(View):
    def get(self, request):
        return HttpResponse("welcome to my blog.")

    def post(self, request):
        return HttpResponse("[POST]welcome to my blog.")


class Article(View):
    def get(self, request):
        articles = ArticleModel.objects.all()
        return render(request, "articles.html", {"articles": articles, "form": ArticleForm()})

    def post(self, request):
        # title = request.POST['title']
        # category = request.POST['category']
        # author = request.POST['author']
        # content = request.POST['content']
        # ArticleModel.objects.create(title=title, category=category, author=author, content=content,
        #                             created_at=datetime.now(tz=timezone.utc))
        form = ArticleForm(request.POST)
        form.instance.created_at = datetime.now(tz=timezone.utc)
        form.save()
        return redirect("/blog/articles")


class ArticleDetails(View):
    def get(self, request, id):
        try:
            article = ArticleModel.objects.get(id=id)
            return render(request, 'article_details.html', {'article': article})
        except ArticleModel.DoesNotExist:
            return HttpResponseNotFound()
